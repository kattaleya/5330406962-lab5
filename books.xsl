<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : books.xsl
    Created on : December 11, 2012, 7:12 PM
    Author     : NUN
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>books.xsl</title>
            </head>
            <body>
                <table border="1">
                    <tr>
                        <th>Title</th>
                        <th>Pages</th>
                    </tr>
                    <xsl:for-each select="//rdf:Description">
                        <xsl:sort select="lib:pages" order="ascending" data-type="number"/>
                        <xsl:if test="lib:pages">
                            <tr>
                                <td>
                                    <xsl:value-of select="@about"/>
                                </td>
                                <td>
                                    <xsl:value-of select="lib:pages"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
