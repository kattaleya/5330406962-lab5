<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : twitterXSLT.xsl
    Created on : December 6, 2012, 4:43 PM
    Author     : NUN
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>twitter RSS</title>
            </head>
            <body>
                <h1> Twitter @ 
                    <a> 
                        <xsl:attribute name="href">
                            <xsl:value-of select="rss/channel/link"/>
                        </xsl:attribute>
                        <xsl:value-of select="rss/channel/link"/>
                    </a>
                </h1>
                <table border="1">
                    <tr>
                        <th> Title </th>
                        <th> Publication Date</th>
                      
                    </tr>
                    <xsl:for-each select="rss/channel/item">

                    <tr>
                       
                            <td>
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="guid"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="title"/>
                                </a>
                            </td>
                            <td>
                                <xsl:value-of select="pubDate"/>
                            </td>                       
                    </tr>
                    </xsl:for-each>

                </table>                      
                            
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
